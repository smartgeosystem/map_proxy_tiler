#!/usr/bin/env bash


# MAIN VARS
export DISTRIB_PATH=$( dirname "$(realpath $0)" )

export INSTALL_PATH=/opt/tiler
export VENV_PATH=$INSTALL_PATH/env

export WORK_PATH=/opt/tiler
export TILES_PATH=/opt/tiles

export EXTERNAL_URL=http://geo3d.scanex.ru:8090


echo "Create dirs..."
mkdir -p $INSTALL_PATH
mkdir -p $WORK_PATH/{in,out,proc,error}
mkdir -p $TILES_PATH

echo "Create virtual env..."
virtualenv $VENV_PATH

echo "Install..."
$VENV_PATH/bin/pip install $DISTRIB_PATH/map_proxy_tiler

echo "Create MapProxy configs..."
$VENV_PATH/bin/mapproxy-util create -t base-config $INSTALL_PATH

echo "Copy and setup config file..."
envsubst < $DISTRIB_PATH/config.yaml.tmpl > $INSTALL_PATH/config.yaml

echo "Copy and setup systemd files..."
mkdir -p $INSTALL_PATH/startup_scripts
envsubst < $DISTRIB_PATH/startup_scripts/mapproxy.service > $INSTALL_PATH/startup_scripts/mapproxy.service
envsubst < $DISTRIB_PATH/startup_scripts/mapproxy_tiler.service > $INSTALL_PATH/startup_scripts/mapproxy_tiler.service
envsubst < $DISTRIB_PATH/startup_scripts/mapproxy_tiler_web.service > $INSTALL_PATH/startup_scripts/mapproxy_tiler_web.service


echo "Create systemd links..."
ln -s $INSTALL_PATH/startup_scripts/mapproxy.service /etc/systemd/system/mapproxy.service
ln -s $INSTALL_PATH/startup_scripts/mapproxy_tiler.service /etc/systemd/system/mapproxy_tiler.service
ln -s $INSTALL_PATH/startup_scripts/mapproxy_tiler_web.service /etc/systemd/system/mapproxy_tiler_web.service


# TODO: CHECK THIS ON REAL SYSTEM:
# export GDAL_BUILD_DIR=/tmp/installer/gdal
if [[ $GDAL_BUILD_DIR ]]; then
    echo "Copy GDAL files to VENV..."
    /bin/cp -rf $GDAL_BUILD_DIR/* $VENV_PATH/
    echo "Fix lib64..."
    bin/cp -rf $GDAL_BUILD_DIR/lib64/* $VENV_PATH/lib/
    echo "Setup LD PATH in service files..."
    sed -i "s|#Environment=|Environment=|g" $INSTALL_PATH/startup_scripts/mapproxy_tiler.service
    echo "Change full path to gdal2tiles..."
    sed -i "s|gdal2tiles_path: gdal2tiles.py|gdal2tiles_path: $VENV_PATH/bin/python $VENV_PATH/bin/gdal2tiles.py|g" $INSTALL_PATH/config.yaml
fi