import shlex
import sys
import traceback
from os import listdir, path, mkdir
from shutil import move
from time import sleep
from subprocess import check_output, CalledProcessError, STDOUT

from huey import crontab
from huey.contrib.sqlitedb import SqliteHuey

from .mapproxy_config_editor import MapproxyConfigEditor
from .config import Config

config = Config()
huey = SqliteHuey('map_proxy_tiler', result_store=False, store_errors=False, filename=config.queue['db_path'])

def get_unic_file_name(file_name):
    # check exists of such filename
    if not path.exists(get_proc_path(file_name)) and \
       not path.exists(get_out_path(file_name)) and \
       not path.exists(get_error_path(file_name)) and \
       not path.exists(get_tiles_path(file_name)):
        return file_name
    # add suffix for name
    suffix_counter = 1
    while suffix_counter < 100:
        suffix = '_{0:02d}'.format(suffix_counter)
        new_file_name = '{0}{1}{2}'.format(path.splitext(file_name)[0], suffix, path.splitext(file_name)[1])
        if not path.exists(get_proc_path(new_file_name)) and \
           not path.exists(get_out_path(new_file_name)) and \
           not path.exists(get_error_path(new_file_name)) and \
           not path.exists(get_tiles_path(new_file_name)):
            return new_file_name
        suffix_counter += 1
    # wtf?
    return file_name


def get_in_path(raster_name):
    return path.join(config.work_dirs['in_path'], raster_name)


def get_proc_path(raster_name):
    return path.join(config.work_dirs['processing_path'], raster_name)


def get_out_path(raster_name):
    return path.join(config.work_dirs['out_path'], raster_name)


def get_error_path(raster_name):
    return path.join(config.work_dirs['error_path'], raster_name)


def get_tiles_path(raster_name):
    return path.join(config.work_dirs['tiles_path'], raster_name)



@huey.periodic_task(crontab(minute="*"))
def check_input_dir():
    for file_name in listdir(config.work_dirs['in_path']):
        fname, fext = path.splitext(file_name)
        if fext.replace('.', '') in config.tiler['ext']:
            new_file_name = get_unic_file_name(file_name)
            pipeline = tile_raster.s(file_name, new_file_name) \
                            .then(update_mapproxy_config, new_file_name) \
                            .then(restart_mapproxy)
            huey.enqueue(pipeline)


@huey.task()
def tile_raster(raster_name, new_raster_name):
    print('Tile raster: ', raster_name)
    try:
        # move to proc dir
        in_path = get_in_path(raster_name)
        proc_path = get_proc_path(new_raster_name)
        move(in_path, proc_path)

        # make dir for tiles
        tiles_path = get_tiles_path(new_raster_name)
        mkdir(tiles_path)

        # tile
        args = shlex.split(config.tiler['gdal2tiles_path'])
        args.extend([
            '--profile', 'mercator',
            '-z', '{0}-{1}'.format(config.tiler['min_zoom'], config.tiler['max_zoom']),
            proc_path,
            tiles_path
        ])
        output = check_output(args, stderr=STDOUT)

        # move to out dir
        out_path = path.join(config.work_dirs['out_path'], new_raster_name)
        move(proc_path, out_path)

    except CalledProcessError as e:
        # move to error dir
        error_path = path.join(config.work_dirs['error_path'], new_raster_name)
        if path.exists(proc_path):
            move(proc_path, error_path)
        elif path.exists(in_path):
            move(in_path, error_path)

        error_log_path = path.join(config.work_dirs['error_path'], new_raster_name + '.log')
        f = open(error_log_path, 'a')
        f.write(str(e.output))
        f.flush()
        raise e

    except Exception as ex:
        # move to error dir
        error_path = path.join(config.work_dirs['error_path'], new_raster_name)
        if path.exists(proc_path):
            move(proc_path, error_path)
        elif path.exists(in_path):
            move(in_path, error_path)

        # write error
        error_log_path = path.join(config.work_dirs['error_path'], new_raster_name + '.log')
        f = open(error_log_path, 'a')
        exc_type, exc_value, exc_traceback = sys.exc_info()
        f.write(str(exc_type))
        f.write(str(exc_value))
        f.writelines(traceback.format_tb(exc_traceback))
        f.flush()
        raise ex


@huey.task(retries=5, retry_delay=1)
@huey.lock_task('update_mapproxy_config')
def update_mapproxy_config(raster_name):
    print('Update config: ', raster_name)
    config_editor = MapproxyConfigEditor(config.map_proxy['config_path'])
    config_editor.append_cache(raster_name, get_tiles_path(raster_name))
    config_editor.append_layer(raster_name, get_tiles_path(raster_name))


@huey.task(retries=5, retry_delay=1)
@huey.lock_task('restart_mapproxy')
def restart_mapproxy():
    print('Restart MapProxy')
    sleep(2)