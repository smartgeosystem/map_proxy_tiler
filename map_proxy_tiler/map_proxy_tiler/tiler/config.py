from yaml import load, Loader
from os import environ, path

DEFAULT_FILE_NAME = 'config.yaml'

class Config:
    def __init__(self):
        config_file_path = None

        # Try to get config file from ENV var
        if 'TILER_CONFIG' in environ:
            config_file_path = environ['TILER_CONFIG']

        # Else try to search locally
        if not config_file_path:
            if path.exists(DEFAULT_FILE_NAME):
                config_file_path = DEFAULT_FILE_NAME

        if not config_file_path:
            proj_path = path.join(path.dirname(path.realpath(__file__)), path.pardir, DEFAULT_FILE_NAME)
            if path.exists(proj_path):
                config_file_path = proj_path

        config_file = open(config_file_path, 'r')
        self.configuration = load(config_file, Loader)

    def __getattr__(self, item):
        return self.configuration.get(item)
