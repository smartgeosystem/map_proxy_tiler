from yaml import load, Loader, dump
from os import path


class MapproxyConfigEditor:
    def __init__(self, mapconfig_path):
        self.mapconfig_path = mapconfig_path

    def _read_config(self):
        with open(self.mapconfig_path, 'r') as mapconfig_file:
            return load(mapconfig_file, Loader)

    def _write_config(self, config):
        with open(self.mapconfig_path, 'wt') as mapconfig_file:
            output = dump(config)
            mapconfig_file.write(output)

    def get_cache_name(self, file_name):
        return file_name + '_cache'

    def get_layer_name(self, file_name):
        return file_name + '_layer'


    def append_cache(self, file_name, cache_path):
        config = self._read_config()
        caches = config["caches"]

        new_cache = dict(
            grids=["GLOBAL_MERCATOR"],
            sources=[],
            cache=dict(
                type='file',
                directory_layout='tms',
                directory=cache_path
            )
        )
        caches[self.get_cache_name(file_name)] = new_cache
        self._write_config(config)

    def append_layer(self, file_name, cache_path):
        config = self._read_config()
        layers = config["layers"]
        new_layer = dict(
            name=self.get_layer_name(file_name),
            title=self.get_layer_name(file_name),
            sources=[self.get_cache_name(file_name),],
        )
        layers.append(new_layer)
        self._write_config(config)

    def get_layers(self):
        config = self._read_config()
        layers = config["layers"]
        return layers


