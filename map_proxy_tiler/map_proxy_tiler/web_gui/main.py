import os
import re
from datetime import datetime
import time
from bottle import route, view, run, static_file, request, TEMPLATE_PATH
from map_proxy_tiler.tiler.config import Config
from map_proxy_tiler.tiler.mapproxy_config_editor import MapproxyConfigEditor

CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_PATH.insert(0, os.path.join(CURRENT_PATH, "views"))


class Task:
    name = None
    size = None
    status = None

    def __init__(self, name, size, status):
        self.name = name
        self.size = size
        self.status = status


def get_tasks():
    tasks = []
    config = Config()
    dir_maps = {
        config.work_dirs['out_path']: 'success',
        config.work_dirs['error_path']: 'failure',
        config.work_dirs['in_path']: 'waiting',
        config.work_dirs['processing_path']: 'processing',
    }

    for dir_path, status in dir_maps.items():
        for file_name in os.listdir(dir_path):
            fname, fext = os.path.splitext(file_name)
            if fext.replace('.', '') in config.tiler['ext']:
                size = os.path.getsize(os.path.join(dir_path, file_name))/(1024*1024)
                tasks.append(
                    Task(
                        name=file_name,
                        size=round(size, 2),
                        status=status
                    )
                )
    return tasks


def get_services():
    services = []
    config = Config()
    mapproxy_config = MapproxyConfigEditor(config.map_proxy['config_path'])
    layers = mapproxy_config.get_layers()
    for layer in layers:
        layer['xyz_url'] = '/tiles/%s/GLOBAL_MERCATOR/{z}/{x}/{y}.png' % layer['name']
        layer['tms_url'] = '/tms/1.0.0/%s/GLOBAL_MERCATOR/{z}/{x}/{y}.png' % layer['name']
        layer['wmts_url'] = '/wmts/%s/GLOBAL_MERCATOR/{z}/{x}/{y}.png' % layer['name']
        if 'timestamp' not in layer.keys():
            layer['timestamp'] = 0
        services.append(layer)
    services.sort(key=lambda x: x['name'].lower())
    return services


def get_external_url():
    config = Config()
    return config.map_proxy['external_url']


def filter_services(req, service_url_param):
    ext_url = get_external_url()
    services = get_services()
    output = []

    hours = req.query.get('hours', None)
    if hours == None:
        ts = -1
    else:
        hours = int(hours)
        dt = datetime.now()
        ts = time.mktime(dt.timetuple())-hours*3600

    mask = req.query.get('mask', '.*')

    for service in services:
        if (int(service['timestamp']) >= ts) and not (re.search(mask, service['name']) is None):
            output.append({
                    'name': service['name'],
                    'url': ext_url + service[service_url_param],
                    'timestamp': service['timestamp']
            })
    return output

@route('/api/services/tms')
def api_services_tms():
    output = filter_services(request, 'tms_url')
    return {'services': output}


@route('/api/services/xyz')
def api_services_xyz():
    output = filter_services(request, 'xyz_url')
    return {'services': output}


@route('/api/services/wmts')
def api_services_wmts():
    output = filter_services(request, 'wmts_url')
    return {'services': output}


@route('/')
@view('tasks')
def index():
    return {'tasks': get_tasks()}

@route('/services')
@view('services')
def index():
    return {'services': get_services(), 'external_url': get_external_url()}


@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=os.path.join(CURRENT_PATH, 'static/'))

@route('/log/<filename:path>')
def serve_logs(filename):
    config = Config()
    error_base_path = config.work_dirs['error_path']
    return static_file(filename + '.log', root=error_base_path)

run(host='0.0.0.0', port=8091)