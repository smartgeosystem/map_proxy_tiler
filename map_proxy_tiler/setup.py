from setuptools import setup, find_packages

setup(name='map_proxy_tiler',
      version='0.1',
      description='Small daemon & web gui for tile rasters and publicate with MapProxy',
      url='https://gitlab.com/smartgeosystem/map_proxy_tiler',
      author='Evgeniy Nikulin',
      author_email='enikulin@smartgeosystem.ru',
      license='GPL',
      packages=find_packages(),
      install_requires=[
            'huey[backends]',
            'bottle',
            'pyyaml',
            'MapProxy'
      ],
      zip_safe=False,
      include_package_data=True
)
