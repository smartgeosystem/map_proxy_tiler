#!/usr/bin/env bash

# Startup script for docker (without systemd)

# DIRS SETUP
export INSTALL_PATH=/opt/mapproxy_tiler
export VENV_PATH=$INSTALL_PATH/env
export WORK_PATH=/opt/tiler

# CONFIG PATH
export TILER_CONFIG=$INSTALL_PATH/config.yaml

# SETUP
export LD_LIBRARY_PATH=$VENV_PATH/lib


# CHECK DIR EXISTS
mkdir -p $WORK_PATH/{in,out,proc,error}

# STARTUP
nohup $VENV_PATH/bin/mapproxy-util serve-develop -b 0.0.0.0:8090 $INSTALL_PATH/mapproxy.yaml &>/var/log/mapproxy.log &
nohup $VENV_PATH/bin/python $VENV_PATH/lib/python2.7/site-packages/map_proxy_tiler/web_gui/main.py &>/var/log/tiler_web.log &
$VENV_PATH/bin/huey_consumer.py map_proxy_tiler.tiler.tasks.huey -q -w 2