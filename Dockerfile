FROM centos:7 as builder

RUN set -x \
    && yum install -y epel-release \
    && yum update -y \
    && yum install -y  wget gcc-c++ gcc hdf-devel hdf5-devel proj proj-devel proj-epsg swig python-devel make \
    && wget -O /tmp/gdal.tar.gz http://download.osgeo.org/gdal/2.4.0/gdal-2.4.0.tar.gz

RUN cd /tmp \
    && tar xzf /tmp/gdal.tar.gz \
    && cd /tmp/gdal-2.4.0 \
    && ./configure --with-hdf4 --with-hdf5 --with-proj --with-grib --prefix /build --with-python \
    && make -j 6 \
    && make install



FROM centos:7 as prod

ENV \
    GDAL_BUILD_DIR=/tmp/installer/gdal \
    WORK_PATH=/opt/tiler \
    TILES_PATH=/opt/tiles

RUN set -x \
    # Install deps
    && yum install -y epel-release \
    && yum update -y \
    && yum install -y  hdf hdf5 proj proj-epsg python python-virtualenv gettext \
    && yum clean all


COPY --from=builder /build /tmp/installer/gdal
COPY . /tmp/installer/map_proxy_tiler

    # Install tiler
RUN /tmp/installer/map_proxy_tiler/install_centos7.sh \
    # Add startup script
    && cp /tmp/installer/map_proxy_tiler/docker-entrypoint.sh /opt/docker-entrypoint.sh

VOLUME $WORK_PATH
VOLUME $TILES_PATH

EXPOSE 8090
EXPOSE 8091

ENTRYPOINT '/opt/docker-entrypoint.sh'



